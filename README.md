README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Romanian, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Romanian data result from an update and an extension of the Romanian part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367).
For the changes with respect to the 1.2 version, see the change log below.

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

<!--
**Raw corpora (UD-parsed) for the 1.2 edition**
------
The raw corpus for the PARSEME 1.2 shared task contains 447,464 sentences from online Romanian newspapers. 
All texts were shuffled so that the original articles cannot be recovered.
The corpus is tokenized into 12,822,588 tokens and parsed with UDPIPE (http://ufal.mff.cuni.cz/udpipe). 
The model used is `romanian-rrt-ud-2.5-191206` (available at https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3131). 
The corpus is split into 100 files of almost the same size.
The corpus processing was performed by Elena Irimia.
-->

**Data annotated for verbal MWEs**

The verbal MWE annotated data for the 1.2 edition corresponds to the 1.1 edition,
with some morpho-syntactic annotation corrections performed mainly with respect to verbal MWEs.

Corpora
-------
All annotated data comes from the "Agenda" newspaper. Some of them are part of the Romanian Universal Dependencies treebank (RoRefTrees, RRT). For this edition of the shared task, the corpus only underwent annotation improvement.


Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated (UDPipe).
* UPOS (column 4): Available. Automatically annotated (UDPipe).
* XPOS (column 5): Available. Automatically annotated (UDPipe).
* FEATS (column 6): Available. Automatically annotated (UDPipe).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated (UDPipe).
* DEPS (column 8): Available. Automatically annotated (UDPipe).
* MISC (column 10): No-space information available. Automatically annotated.
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: IAV, VID, LVC.full, LVC.cause, IRV.

The UDPipe annotation relied on the model `romanian-rrt-ud-2.10-220711`.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 


Tokenization
------------
The training and testing files are annotated with the TTL tool (developed at ICIA, by Radu Ion, 2007).


Authors
-------
Annotation of IAVs in version 1.3 was performed automatically by Maria Mitrofan, then manually checked by Elena-Andreea Bărbulescu and Bianca-Mădălina Zgreabăn, and finally approved by Verginica Barbu Mititelu and Mihaela Ionescu Cristescu.
All VMWEs annotations for versions 1.1, 1.2 were performed by Verginica Barbu Mititelu and Mihaela Ionescu Cristescu.
Annotation of VMWEs in version 1.0 of the corpus was performed by Verginica Barbu Mititelu, Mihaela Ionescu Cristescu, Mihaela Onofrei, and Monica-Mihaela Rizea


License
-------
The data are distributed under the terms of the CC BY v4 License.


Contact
-------
vergi@racai.ro 

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
    - adding annotations for the IAV category
    - updating the morphosyntactic annotation using UDPipe 2 and the romanian-rrt-ud-2.10-220711 model (done by Agata Savary).
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
  - Changes with respect to the 1.1 version are the following:
    - morpho-syntactic annotation corrections performed mainly with respect to verbal MWEs
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
  - Changes with respect to the 1.0 version are the following:
    - updating the existing VMWE annotations to comply with PARSEME [guidelines eidtion 1.1](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/).
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.

